#pragma once

// this file is going to be small definitions of things that may become useful
// custom error codes etc

enum NTAM_ERR: int32_t
{
    UNK = -1,
    FAIL,
    SUCCESS,
};