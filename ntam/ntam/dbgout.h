#pragma once

// i need a stdout wrapper class
// this could be done a lot better 
// maybe color coding?

class Dbg
{
private:

    Console m_conout;

public:

    Dbg() :
        m_conout("dbg")
    {

    }

    auto& get_console()
    {
        return m_conout;
    }

    template <typename ...Params>
    void err(const std::string& txt, Params&&... params)
    {
        m_conout.print("[ntam-ERR] ");
        m_conout.print(txt, std::forward<Params>(params)...);
        m_conout.print("\n");
    }
    
    template <typename ...Params>
    void out(const std::string& txt, Params&&... params)
    {
        m_conout.print("[ntam] ");
        m_conout.print(txt, std::forward<Params>(params)...);
        m_conout.print("\n");
    }
};

extern Dbg g_dbg;