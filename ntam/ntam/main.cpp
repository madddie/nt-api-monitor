#include "inc.h"

Dbg g_dbg{};
Proc_Handler g_proc{};

//
// ntam.exe proc.exe -arg1 -arg2 -arg3
// ...
// specify further arguments?
// ...
//
NTAM_ERR parse_args(int argc, char** argv)
{
    // how many args are there
    g_dbg.out("argc %d", argc);

    if (argc < 2)
    {
        g_dbg.err("missing required argument \"filepath.exe\"");

        return NTAM_ERR::FAIL;
    }

    // make filesystem::path 
    auto path = fs::path(argv[1]);

    // check if its rel or abs
    auto rel = path.is_relative();

    // if its relative, prepend the current directory
    // not really sure how necessary all this work is?
    if (rel)
    {
        std::wstringstream fullpath{};
        wchar_t pathbuf[MAX_PATH]{};
        GetModuleFileNameW(0, pathbuf, MAX_PATH);

        std::wstring pathstr = pathbuf;
        pathstr.erase(pathstr.find_last_of(L'\\') + 1);
        fullpath << pathstr << util::multibyte_to_wide(argv[1]) << L".exe";


        path = fs::path(fullpath.str());
    }

    // setup the filepath for our proc handler
    if (g_proc.init_path(path) != NTAM_ERR::SUCCESS)
        return NTAM_ERR::FAIL;

    // stack up all the individual arguments for our process
    for (int i = 1; i <= (argc-1); ++i)
    {
        g_dbg.out("%s", argv[i]);

        g_proc.add_arg(argv[i]);
    }

    return NTAM_ERR::SUCCESS;
}

int main(int argc, char** argv)
{
    g_dbg.out("starting...");

    // handle args
    // we need an exe to run lol
    if (parse_args(argc, argv) != NTAM_ERR::SUCCESS)
    {
        g_dbg.err("unable to parse arguments");

        return NTAM_ERR::FAIL;
    }

    // now that we've got a process to log, lets open it!
    if (g_proc.create_process() != NTAM_ERR::SUCCESS)
    {
        g_dbg.err("unable to create process");

        return NTAM_ERR::FAIL;
    }

    std::getchar();

    return NTAM_ERR::SUCCESS;
}