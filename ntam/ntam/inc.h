#pragma once

// main overarching helper file
#include <m_maddie.h>

namespace fs = std::experimental::filesystem;

// definitions we need
#include "def.h"

// need a debug output text spewer
#include "dbgout.h"

// process handler
#include "proc_handler.h"