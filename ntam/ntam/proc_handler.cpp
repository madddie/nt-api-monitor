#include "inc.h"

Proc_Handler::Proc_Handler()
{
    
    // don't need to do anything yet?

}

Proc_Handler::~Proc_Handler()
{

    // handle closing/detach of file

}

Proc_Handler::Proc_Handler(const fs::path & bin)
{
    // setup the filepath for our process
    m_path = bin;
}

NTAM_ERR Proc_Handler::init_path(const fs::path & bin)
{
    m_path = bin;

    return NTAM_ERR::SUCCESS;
}

void Proc_Handler::add_arg(const std::string& arg)
{
    m_arguments.push_back(arg);
}

void Proc_Handler::init_args(const std::vector<std::string>& list)
{
    // stack up all the args
    for (auto& it : list)
    {
        m_arguments.push_back(it);
    }
}

NTAM_ERR Proc_Handler::create_process()
{
    // not sure yet
    STARTUPINFOW info{};
    PROCESS_INFORMATION pi{};

    // make the arg list
    std::wstringstream args{};

    // stuff
    for (auto& arg : m_arguments)
    {
        args << util::multibyte_to_wide(arg) << " ";
    }

    g_dbg.out("args %s", util::wide_to_multibyte(args.str().c_str()));
    g_dbg.out("path %s", util::wide_to_multibyte(m_path.c_str()));

    // a lot of the arguments regarding this need special attention paid to them
    // there is a lot that can be done with regard to how an NT process is created and managed
    if (CreateProcessW(
        m_path.c_str(),
        (LPWSTR)args.str().c_str(),
        nullptr,
        nullptr,
        TRUE, // ?
        0, // this could be pretty gangster
        nullptr,
        nullptr,
        &info,
        &pi
    ) == 0)
    {
        g_dbg.err("couldn't create process");
        g_dbg.err("last NT errror %d", GetLastError());

        return NTAM_ERR::FAIL;
    }

    return NTAM_ERR::SUCCESS;
}
