#pragma once


class Proc_Handler
{
private:
    HANDLE m_proc_handle;

    fs::path m_path;

    std::vector<std::string> m_arguments;

public:

    // idk what these will be yet
    Proc_Handler();
    ~Proc_Handler();

    // ctor by filepath
    Proc_Handler(const fs::path& bin);
    
    // init the path
    NTAM_ERR init_path(const fs::path& bin);

    // add a single arg
    void add_arg(const std::string & arg);

    // grab the arguments for the app we want to log
    void init_args(const std::vector<std::string>& list);

    // create process
    NTAM_ERR create_process();

    



};

extern Proc_Handler g_proc;

