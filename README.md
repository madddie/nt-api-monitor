| working title
- nt api monitor
- ntam.exe

| constraints
- usermode only!
- internal for now
- external client perhaps later, likely requires dkom/hook into kernel callback routines.

| theoretical usage - () req. - [] opt.
| todo, finish what this is supposed to mean
- ntam (/bin.exe) [-/log_output.txt] [-type_log] [-filter_keywords]

| goals
- executes/lite-contains software on windows
- spawns instance(s) of application 
- spawns debug window with logger output (monitor)

| thoughts
- hook all nt imports and jumps each thru our own logger
- attach some type of low-level NT monitor 
- look into undocumented NT internals

| log safety and type/relation of calls
- thread/queue
- memory rwx/rights
- alloc
- createfile
- createprocess


